# **vault migration**

A tool to leverage the Vault CLI or API in the backend that can take the source credentials as input, extract the existing mounts and paths and can set all the permissions needed to redirect the existing LDAP credentials to custom namespace.

## **Kick-off the migration job**


[Click here](https://gitlab.com/yasaswy22/vault-migration/-/pipelines/new) to start the migration.


Note: Modify the code to your client hosted Vault URL and your project needs. This code will not run as is without the proper working Vault hosted URL. They are masked in the current repository.
If you are a developer, to stop the running of the pipeline on each commit use [ci skip] text in the commit message.

----

## **Project info: Vault Migration using Gitlab Runners**

## Problem description

Migration to vault as an enterprise direction in the client started a couple of years ago when the applications started moving from on-prem servers to Cloud Foundry servers as a part of cloud modernization initiative. Credentials to access Vault were created by Vault Admin team and access is controlled by LDAP credentials and anyone who needed to modify or read the credentials need to get the access to the group. A login to these LDAP credentials would lead us to root and we can store our secrets needed for the application integration and end to end flow behind in this mount/
path created in the root for the individual teams.

<br>

![Existing State](/README-content/existing_state_1.JPG "Existing State")

<br>

This design led to almost all teams having their credentials in root and none of the teams taking advantage of the Vault Namespace feature. Vault Namespaces can be created by teams, and they can also create nested namespaces for smaller team within their area for better organization of their credentials.
We can recreate these manually under a Team Namespace, but that is not a recommended approach due to the following reasons.
-	Manual migration of the credentials can lead to path problems, break the code.
-	There would be no trace if someone creates these manually.
-	Separate team needed to do this work while we go through submission of forms.

<br>

## Solution Summary

Designed and developed a tool to leverage the Vault CLI or API in the backend that can take the source credentials as input, extract the existing mounts and paths and can set all the permissions needed to redirect the existing LDAP credentials to custom namespace.

<br>

![Desired State](/README-content/desired_state_1.JPG "Desired State")

<br>

This tool will kick start a workflow that runs in Gitlab using the shared Gitlab runner machines whenever there is a new request for migration. It will automatically setup the vault permissions like the necessary policies, roles and identity group required for the requested LDAP credential migration. This tool has a lot of flexibility to request fewer inputs and customize it to a particular namespace to the team or it can keep it open with a wide range of variables that can migrate it to any namespace requesting a few more details from the user.

<br>

## Solution Details

### Gitlab Runner Pipeline Setup

<br>

This is a one-time setup needed to give access to the pipeline to create the new secrets, policies, roles, identity groups whenever a new request is submitted by the end user.
Following are the steps to create a new JWT auth method for the pipeline setup.

<br>

```
1.	Create a policy admin-vault-migration-policy
2.	In the Vault UI go to Access --> Auth methods --> Select type: JWT --> Path: jwt-gitlab-vault-migration
3.	Open a new gitbash terminal after logging in as admin, to run the below commands
4.	vault write auth/ jwt-gitlab-vault-migration/config jwks_url=https://xxgitlab.opr.company.org/-/jwks bound_issuer=”xxgitlab.opr.company.org”
5.	vault auth tune -audit-non-hmac-request-keys=role -audit-non-hmac-response-keys-error -max-lease-ttl=60m jwt-gitlab-vault-migration
6.	vault write auth/ jwt-gitlab-vault-migration/role/ vault-migration-admin-role - <<EOF
{
	"role_type": "jwt",
	"policies": ["admin-vault-migration-policy"],
	"token_ttl": 900,
	"token_max_ttl": 3600,
	"user_claim": "project_id",
	"bound_claims": {
		"project_id": ["124398"]
	}
}
EOF
```
<br>

`project_id` above is the gitlab project id shown under the name in the gitlab repo.
This will give all the permissions needed for the Gitlab Runner jobs to create new secrets and permissions needed to access them. If there are any changes in the policy delete the role and recreate using the above in step 6. You can delete using the below command
vault delete auth/ jwt-gitlab-vault-migration/role/ vault-migration-admin-role
We are all set to create the job to our business need.

<br>

Following diagram shows the key stages involved in the solution.

<br>

![Pipeline Stages](/README-content/pipeline_stages_1.JPG "Pipeline Stages")

<br>

Dependencies and further integration flow of jobs are shown below.

<br>

![Pipeline Needs](/README-content/pipeline_needs_1.jpg "Pipeline Needs")

<br>

### 1.	Get Entity Details

Existing vault secrets are in vault /root and there are live test and production applications that are using the secrets in the /mount and /path residing in the /root. To relink the LDAP credentials from /root to /namespace we need to first extract the entity id and use this entity_id to create the new connection.

<br>

```
Inputs: vault address, ldap id, ldap password
Outputs: entity id, root vault token (expiry in 60 mins)
```

<br>

```
get-entity:
  stage: extract-source-details 
  script:
    - export VAULT_ADDR=$VAULT_TEAM_ADDR 
    - vault login -method=ldap username=$LDAP_ID password=${LDAP_PASSWORD}
    - vault token lookup -format='json' > source_details.json 
    - export entity_id=$(jq -r '.data.entity_id' source_details.json)
    - echo $entity_id > entity.txt
    - export VAULT_TOKEN=$(jq -r '.data.id' source_details.json)
    - echo $VAULT_TOKEN > vault_token_root.txt 
  artifacts: 
    paths:
      - entity.txt
      - vault_token_root.txt 
    expire_in: 60 mins
```

### 2.	Get Secrets

We extract the actual application secrets in this stage from the /root and keep them ready in artifacts so we can use the same details to create in /namespace location. 

<br>

```
Inputs: vault address, root token cached in the vault_token_root.txt Artifact.
Outputs: Application secrets (expiry in 5 mins)
```

<br>

```
get-secrets:
  stage: extract-source-details
  needs: [get-entity]
  script:
    - export VAULT_ADDR=$VAULT_TEAM_ADDR
    - export VAULT_TOKEN=$(cat vault_token_root.txt)
    - vault kv get -format='json' $VAULT_CUSTOM_PATH/$ENV_NAME > secrets_source.json
    - jq '.data' secrets_source.json > secrets_kv.json
  artifacts:
    paths:
      - secrets_kv.json
    expire_in: 5 mins
```

### 3.	Initializing the Team’s Namespace

We login to the namespace extract the namespace token here and cache it in the Artifacts for further use in the upcoming stages.

<br>

```
Inputs: vault address, vault namespace, vault team auth method, vault team role.
Outputs: vault namespace token (expiry in 60 mins)
```

<br>

```
vault-destination-init:
  stage: init-destination-details
  needs: [get-entity]
  script:
    - export VAULT_ADDR=$VAULT_TEAM_ADDR
    - export VAULT_NAMESPACE=$VAULT_TEAM_NAMESPACE
    - export VAULT_TOKEN="$(vault write-field-token auth/$VAULT TEAM_AUTH_METHOD//login role $VAULT_TEAM ROLE jwt=$CI JOB JWT)"
    - echo $VAULT_TOKEN > vault_token_namespace.txt
  artifacts:
    paths:
      - vault_token_namespace.txt
    expire_in: 60 mins
```

### 4.	Creating the secrets in Team’s Namespace

In this stage we take the cached secrets create them for further use which is the whole point of this migration effort. Going forward the /root secrets would be removed and /namespace secrets are the only source of application secrets.

<br>

```
Inputs: vault address, vault namespace address, vault namespace token cached in the Artifact vault_token_namespace.txt, vault mount, vault path.
Outputs: None
```

<br>

```
put-secrets:
  stage: init-destination-details
  needs: [get-secrets, vault-destination-init]
  script:
    - export VAULT_ADDR=$VAULT_TEAM_ADDR
    - export VAULT_NAMESPACE=$VAULT_TEAM_NAMESPACE
    - export VAULT_TOKEN=$(cat vault_token_namespace.txt)
    - vault kv put $VAULT_CUSTOM_MOUNT/SVAULT_CUSTOM_PATH/SENV_NAME @secrets_kv.json
```

### 5.	Creating the policy for secret’s access in Team’s Namespace

Above secrets newly created in Team’s Namespace should be accessed by the application. This is controlled by a vault policy. We write the policy in this stage and use it in further stages.

<br>

```
Inputs: vault address, vault namespace address, vault namespace token cached in the Artifact vault_token_namespace.txt, vault mount, vault path, CF org
Outputs: Policy document
```

<br>

```
create-policy:
  stage: create-permissions
  needs: [put-secrets, vault-destination-init]
  script:
    - export VAULT_ADDR=$VAULT_TEAM_ADDR
    - export VAULT_NAMESPACE=$VAULT_TEAM_NAMESPACE
    - export VAULT_TOKEN=$(cat vault_token_namespace.txt)
    - |
      cat <<EOF > read_policy.hcl 
      path "${VAULT_CUSTOM_MOUNT}/data/${VAULT_CUSTOM_PATH} /*" {
        capabilities = [ "read", "list"]
      }
      EOF
    - vault policy write ${ORG}-${VAULT_CUSTOM_MOUNT}-read-policy read_policy.hcl
  artifacts:
    paths:
      - read_policy.hcl
    expire_in: 60 mins
```

### 6.	Creating the role and secret id

In this stage we need to create the role using the above policy, so the LDAP id is assigned this role to read the secrets. We also create the secret_id here to use role_id and secret_id to login as the LDAP id

<br>

```
Inputs: vault address, vault namespace address, vault namespace token cached in the Artifact vault_token_namespace.txt, vault mount, vault path, CF org
Outputs: None
```

<br>

```
create-role-secret:
  stage: create-permissions
  needs: [create-policy]
  script:
    - export VAULT_ADDR=$VAULT_TEAM_ADDR
    - export VAULT_NAMESPACE=$VAULT_TEAM_NAMESPACE
    - export VAULT_TOKEN=$(cat vault_token_namespace.txt)
    - vault write auth/approle/role/${ORG) -${VAULT_CUSTOM_MOUNT) -read-role secret_id_ttl=24h token_num_uses=2 token_ttl=20m token_max_ttl-24h secret_id_num_uses=40 policies=${ORG} -${VAULT_CUSTOM_MOUNT}-read-policy token_policies=${ORG} -${VAULT_CUSTOM_MOUNT}-read-policy
    - vault read -format='json' auth/approle/role/${ORG} -${VAULT_CUSTOM_MOUNT}-read-role/role-id > role-read.json 
    - jq -r '.data.role_id' role-read.json > role-id.txt 
    - vault write -f -format='json' auth/approle/role/${ORG}-${VAULT_CUSTOM_MOUNT}-read-role/secret-id > secret-read.json 
    - jq -r '.data.secret_id' secret-read.json > secret-id.txt 
    - vault write auth/approle/login role_id=$(cat role-id.txt) secret_id=$(cat secret-id.txt)
```

### 7.	Creating the identity group

In this stage is the final step to attach the LDAP credential to swap if from /root to this /namespace created secrets. After this the applications accessing the secrets should mention the /namespace in the code and no other change is required.

```
Inputs: vault address, vault namespace address, vault namespace token cached in the Artifact vault_token_namespace.txt, entity_id cached in the Artifact entity.txt, vault mount, vault path, CF org
Outputs: None
```

<br>

```
create-identity-group:
  stage: create-permissions
  needs: [get-entity, vault-destination-init, create-policy]
  script:
    - export VAULT_ADDR=$VAULT_TEAM_ADDR
    - export VAULT_NAMESPACE=SVAULT_TEAM_NAMESPACE
    - export VAULT_TOKEN=$(cat vault_token_namespace.txt)
    - vault write identity/group name=$ LDAP_ID policies="${ORG}-${VAULT_CUSTOM_MOUNT}-read-policy" member_entity_ids=$(cat entity.txt) type="internal"
```

## Usage

End user like a Security Analyst for Production or Developer for Test environment will kick start the pipeline with the link given in the README file in the project.

<br>

![Kickoff Pipeline](/README-content/kick-off-pipeline-1.JPG "Kickoff Pipeline")

<br>

It will take you to the page that looks for values for the following variables.

<br>

![Pipeline Variables](/README-content/pipeline-variables-1.JPG "Pipeline Variables")

<br>

Note: You may not have the access to kick off in the repo shared above, clone it to run.

Workflow gets triggered and it navigates through dependencies to complete the migration. Developer can test their application after the successful stages.

<br>

![Pipeline Stages](/README-content/pipeline_stages_1.JPG "Pipeline Stages")

<br>

## Setup Gitlab Runners in IBM Cloud

We need to host the above Gitlab Runner job in a Virtual Server, so it is picked up whenever there is a request for migration from the user. In this case it is ideal to use an IBM Cloud virtual server (Can be used in any cloud like AWS, Azure, GCP but instruction and CLI commands will change)

<br>

![IBM Cloud hosted](/README-content/ibm_cloud_jobs_1.JPG "IBM Cloud hosted")

<br>

### Setting up IBM Cloud account

<br>

1. First get to https://cloud.ibm.com/ and register to create an account.
2. Install IBM Cloud CLI. For Windows™ 10 Pro, run the following command in PowerShell as an administrator: `iex(New-Object Net.WebClient).DownloadString('https://clis.cloud.ibm.com/install/powershell')`
3. To verify the CLI use `ibmcloud help`
4. Install the VPC Infrastructure plugin to create instances `ibmcloud plugin install vpc-infrastructure`. `ibmcloud is` commands will not run without this.
5. You would need the API Key to proceed further with creating the machines through IBM Cloud CLI. Login to IBM Cloud in cloud.ibm.com click on profile icon in the top right corner
click on Login to CLI and API. Pick up the API Key from there
6. In Gitbash or any terminal use the following commands
```
 - export API_KEY="API key Picked above"
 - ibmcloud login --no-region --apikey $API_KEY
 - ibmcloud target -r us-east
 - ibmcloud is vpcc
 - ibmcloud is images #to get the list of images
 - ibmcloud is subnet-create gitlab-subnet r014-82798de6-3bd6-4706-9eb0-cd30d80dbe77 --ipv4-address-count 256 --zone us-east-1
 
 - use puttygen or ssh-keygen to generate a new key
 
 - ibmcloud is key-create ibm-tp-key \"above created public key\"
 - ibmcloud is instance-create\
    gitlab-instance\
    r014-82798de6-3bd6-4706-9eb0-cd30d80dbe77\
    us-east-1\
    bx2-2x8\
    0757-5f925dc3-630e-48f6-b02a-02d4f98446b3\
    --image-id 0738-cc8debe0-1b30-6e37-2e13-744bfb2a0c11\
    --key-ids 1234xxxx-x12x-xxxx-34xx-xx1234xxxxxx\
    --volume-attach @/Users/myname/myvolume-attachment_create.json\
    --placement-group r134-a812ff17-cac5-4e20-8d2b-95b587be6637\
    --metadata-service true
    --host-failure-policy true
 ```
7. Setup docker in the created virtual server. You can use the following commands to install the latest version of Docker from Unicamp.

```
mkdir /root/docker; cd /root/docker
wget https://oplab9.parqtec.unicamp.br/pub/repository/rpm/open-power-unicamp.repo;
sudo mv open-power-unicamp.repo /etc/yum.repos.d/; sudo yum -y update; sudo yum -y install docker-ce;
service docker start
```

### Install Gitlab Runner

<br>

Perform the following steps to install GitLab Runner:

1. Add the official GitLab repository.

```# For Debian/Ubuntu/Mint
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
# For RHEL/CentOS/Fedora
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
```
2. Install the latest version of GitLab Runner.

```# For Debian/Ubuntu/Mint
sudo apt-get install gitlab-runner
# For RHEL/CentOS/Fedora
sudo yum install gitlab-runner
```
3. You can also [download](https://gitlab-runner-downloads.s3.amazonaws.com/latest/index.html) the required package (deb or rpm) for your system.
`curl -Lf https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_ppc64le.rpm`
4. Use the downloaded package to install Gitlab Runner locally.
`yum localinstall gitlab-runner_ppc64le.rpm`

### Register GitLab Runner

Registering a runner is the process of binding the runner with a GitLab instance.
As a prerequisite, you need to create a project and acquire the registration token:

In the  [gitlab repo](https://gitlab.com/yasaswy22/vault-migration), click Settings -> CI/CD. In the Runners section, click Expand to view the GitLab instance URL and token which will be used to register the runner.

1. Run the following command to register the runner.

```
#gitlab-runner register
Runtime platform arch=ppc64le os=linux pid=378397 revision=3574bc9c version=13.12.0~beta.23.g3574bc9c

Running in system-mode.Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
GR1348941kRSya-_-2aP-xxxxxxxx
Enter a description for the runner:
[yasaswy.nagavolu@in.ibm.com]: docker executor for virtual server
Enter tags for the runner (comma-separated):
shared
Registering runner… succeeded runner= JLgUopmM
Enter an executor: custom, docker-ssh, docker+machine, kubernetes, docker, parallels, shell, ssh, virtualbox, docker-ssh+machine:
docker
Enter the default Docker image (for example, ruby:2.6):
alpine:latest
Runner registered successfully. Feel free to start it, but if it’s running already the config should be automatically reloaded!
```

2. After the runner is registered successfully, go to the GitLab repository and click Settings -> CI/CD. In the Runners section, click Expand. The newly registered runner should be listed in the Available specific runners section.

3. In the gitlab-ci.yml file in your GitLab project, make sure to add the runner tag for registering the runner in the tags field (in this example, shared).

You can find the reusable asset in the URL: [gitlab repo](https://gitlab.com/yasaswy22/vault-migration)
For any further questions please reach out to my email id yasaswy.nagavolu@in.ibm.com